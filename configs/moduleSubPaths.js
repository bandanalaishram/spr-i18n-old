const SUB_PATHS = {
  'sprinklr-app': 'ui/web/neo/'
};

module.exports = (moduleName) => {
  return SUB_PATHS[moduleName] || '';
};
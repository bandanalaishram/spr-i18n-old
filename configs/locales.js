module.exports = () => [
  'ja_JP', //Japanese
  'pt_BR', //Portuguese
  'de_DE', //Germany
  'zh_CN', //Chinese
  'ru_RU', //Russian
  'es',    //Spanish
  'fr_FR', //French
  'it_IT',  //Italian,
  'ar_SA', //Arabic
  'ko_KR' //Korean
];
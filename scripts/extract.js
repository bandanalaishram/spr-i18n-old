const gulp = require('gulp');
const plumber = require('gulp-plumber');
const i18next = require('i18next-scanner');
const fs = require('fs');
const shell = require('gulp-shell');
const tap = require('gulp-tap');
const jsonfile = require('jsonfile');
const _forEach = require('lodash/forEach');
const i18nExtract = require('./utils/i18nExtract');
const getDirectories = require('./utils/getDirectories');
const subModules = getDirectories('modules');
const getSubPath = require('../configs/moduleSubPaths');

jsonfile.spaces = 2;

_forEach(subModules, dir => {
  const subPath = getSubPath(dir);
  const packageJson = require(`../modules/${dir}/${subPath}package.json`);

  const filenames = [];
  _forEach(packageJson.i18n, filename => {
    filenames.push(`modules/${dir}/${subPath}${filename}`);
  });
  if(filenames.length <= 0) {
    filenames.push('src/**/*.js');
  }
  i18nExtract(filenames, dir);
});

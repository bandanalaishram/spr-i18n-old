const fs = require('fs');
const jsonfile = require('jsonfile');
const _forEach = require('lodash/forEach');
const _merge = require('lodash/merge');
const getDirectories = require('./utils/getDirectories');
const i18nDirectories = getDirectories('i18n');
jsonfile.spaces = 2;

let obj = {};
let allLabels = {};
_forEach(i18nDirectories, pathname => {
  allLabels = _merge(obj, JSON.parse(fs.readFileSync(`i18n/${pathname}/en_US.json`, 'utf8')));
});
jsonfile.writeFile('i18n/en_US.json', allLabels);
const jsonfile = require('jsonfile');
const _forEach = require('lodash/forEach');
const _get = require('lodash/get');
const shell = require('gulp-shell');
const gulp = require('gulp');
const path = require('path');
const through = require('through2');

const getToken = require('./utils/getToken');

const PACKAGE = require(`${process.env.PWD}/package.json`);
const projectId = _get(PACKAGE, 'i18n.projectId');

const SPANISH_LOCALE_ID = 'es';
const SPANISH_FILENAME = 'es_ES';
const getLocales = require('../configs/locales');
const LOCALES = getLocales();
const tokenProcess = getToken();
const getDirectories = require('./utils/getDirectories');
const subModules = getDirectories('modules');

function download() {
  jsonfile.readFile('smartlingToken.json', (err, tokenObj) => {
    let fileName = '';
    const authToken = _get(tokenObj, 'response.data.accessToken', '');
    if(!authToken) {
      console.log('Auth token not found');
      return;
    }

    LOCALES.map(lang => {
      _forEach(subModules, moduleName => {
        fileName = ( lang === SPANISH_LOCALE_ID ) ? SPANISH_FILENAME : lang;
        const fileUri = `/files/en_US_${moduleName}.json`;
        const desFilePath = `i18n/${moduleName}/${fileName}_${moduleName}.json`;
        downloadFile(desFilePath, fileUri, lang);
      });
    });

    function downloadFile(desFilePath, fileName, lang) {
      const localeCode = lang.replace('_', '-');
      gulp.src('i18n/en_US.json')
        .pipe(shell([
          '[ -e smartlingToken.json ] && rm -- smartlingToken.json;'+
          'curl -X GET -H "Authorization: Bearer ' + authToken + '"' +
          ' "https://api.smartling.com/files-api/v2/projects/' + projectId + '/locales/' + localeCode + '/file?fileUri=' + fileName + '&retrievalType=published"' +
          ' > ' + desFilePath + ''
        ]));
    }
  });
}

tokenProcess.pipe(through.obj(download));

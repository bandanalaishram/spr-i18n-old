const jsonfile = require('jsonfile');
const shell = require('gulp-shell');
const gulp = require('gulp');
const path = require('path');
const _forEach = require('lodash/forEach');
const _get = require('lodash/get');
const PACKAGE = require(`${process.env.PWD}/package.json`);
const through = require('through2');
const projectId = _get(PACKAGE, 'i18n.projectId');
const smartlingUploadUrl = `https://api.smartling.com/files-api/v2/projects/${projectId}/file`;
const getToken = require('./utils/getToken');
const tokenProcess = getToken();
const getDirectories = require('./utils/getDirectories');
const subModules = getDirectories('modules');

function upload() {
  jsonfile.readFile('smartlingToken.json', (err, tokenObj) => {
    const authToken = _get(tokenObj, 'response.data.accessToken', '');
    if (!authToken) {
      console.log('Auth token not found');
      return;
    }

    _forEach(subModules, moduleName => {
      //JSON file upload
      uploadFile(`i18n/${moduleName}/en_US_${moduleName}.json`, `en_US_${moduleName}`, 'json');
    });

    function uploadFile(filePath, file, fileType) {
      gulp.src(filePath)
        .pipe(shell([
          '[ -e smartlingToken.json ] && rm -- smartlingToken.json;'+
          'curl -X POST -H "Authorization: Bearer ' + authToken + '"' +
          ' -F "file=@<%=file.path%>"' +
          ' -F "fileUri=/files/' + file + '"' +
          ' -F "authorize=true"' +
          ' -F "fileType=' + fileType + '" ' +
          smartlingUploadUrl
        ]));
    }
  })
}

tokenProcess.pipe(through.obj(upload));
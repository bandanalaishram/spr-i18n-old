const gulp = require('gulp');
const shell = require( 'gulp-shell' );
const i18next = require('i18next-scanner');
const plumber = require('gulp-plumber');
const tap = require('gulp-tap');
const fs = require('fs');
const path = require('path');
function getRegex(fns) {
  const funcs = '(?:' + fns.join(')|(?:').replace('.', '\\.') + ')';
  const pattern = '[^a-zA-Z0-9_](?:' + funcs + ')(?:\\(|\\s)\\s*(?:(?:\'((?:(?:\\\\\')?[^\']*)+[^\\\\])\')(|\\s*)\\)|(?:"((?:(?:\\\\")?[^"]*)+[^\\\\])")(|\\s*)\\)|(?:`((?:(?:\\\\`)?[^`]*)+[^\\\\])`)(|\\s*)\\))';
  return new RegExp(pattern, 'g');
}

function customTransform(file, enc, done) {
  const regexFns = ['__'];
  const parser = this.parser;
  const content = fs.readFileSync(file.path, enc);
  const results = content.match(getRegex(regexFns));

  if (results) {
    results.forEach((result) => {
      const key = result.match(/\_\_\((\s+|)[\'\"\`](.*(\s|).*)[\'\"\`](\s+|)/)[2];
      if (key) {
        parser.set(key, key);
      }
    });
  }

  done();
}

module.exports = (filenames, dir) => {
  return gulp.src(filenames)
    .pipe(plumber())
    .pipe(tap(file => console.log(`Extracting from ${file.path}`)))
    .pipe(i18next({
      lngs: [''],
      resource: {
        savePath: 'en_US.json',
      },
      ns: 'en_US',
      keySeparator: null,
      nsSeparator: false       //   https://github.com/i18next/i18next-scanner/issues/31
    }, customTransform))
    .pipe(plumber.stop())
    .pipe(gulp.dest(`i18n/${dir}`))
};

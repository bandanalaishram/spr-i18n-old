const fs = require('fs');
const path = require('path');

module.exports = (directory) => {
  const folders =  fs.readdirSync(directory)
    .filter(function(file) {
      return fs.statSync(path.join(directory, file)).isDirectory();
    });
  return folders;
};
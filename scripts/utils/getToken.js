const gulp = require('gulp');
const shell = require( 'gulp-shell' );
const userIdentifier = 'WDMSNbjFPIyYdsEcBEIVcblZwDroXe';
const userSecret = 'ng1euu743s5mmeiorqshdq1tg9Ry[bd9gcld0gkt5vsok874fm93oks';

module.exports = () => {
  return gulp.src( `i18n/en_US.json` )
    .pipe( shell( [
      'curl -X POST -H "Content-Type: application/json" -d \'{' +
      '"userIdentifier": "'+userIdentifier+'", ' +
      '"userSecret": "'+userSecret+'"' +
      '}\' https://api.smartling.com/auth-api/v2/authenticate > smartlingToken.json'
    ] ) );
};

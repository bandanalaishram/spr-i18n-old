const fs = require('fs');
const jsonfile = require('jsonfile');
const _forEach = require('lodash/forEach');
const _get = require('lodash/get');

const PACKAGE = require(`${process.env.PWD}/package.json`);

const projectName = _get(PACKAGE, 'i18n.projectName');
const SPANISH_LOCALE_ID = 'es';
const SPANISH_FILENAME = 'es_ES';
const getDirectories = require('./utils/getDirectories');
const subModules = getDirectories('modules');
const getLocales = require('../configs/locales');
const LOCALES = getLocales();
jsonfile.spaces = 2;

_forEach(subModules, moduleName => {
  //updating json labels
  LOCALES.map(filename => {
    const curFileName = (filename === SPANISH_LOCALE_ID) ? SPANISH_FILENAME : filename;
    jsonfile.readFile(`i18n/${moduleName}/${curFileName}_${moduleName}.json`, (err, translatedFile)=> {
      jsonfile.readFile(`i18n/${curFileName}.json`, (err, currentFile) => {
        _forEach(translatedFile, (val, key) => currentFile[key] = val);
        jsonfile.writeFile(`i18n/${curFileName}.json`, currentFile, (err) => {
        });
      });
    });
  });
});

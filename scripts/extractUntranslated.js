const fs = require('fs');
const plumber = require('gulp-plumber');
const gulp = require('gulp');
const jsonfile = require('jsonfile');
const _forEach = require('lodash/forEach');
const _reduce = require('lodash/reduce');
const _isUndefined = require('lodash/isUndefined');
const SPANISH_FILENAME = 'es_ES';
const SPANISH_LOCALE_ID = 'es';
const getLocales = require('../configs/locales');
const LOCALES = getLocales();
const getDirectories = require('./utils/getDirectories');
const subModules = getDirectories('modules');

jsonfile.spaces = 2;

_forEach(subModules, moduleName => {

  //Extracting Labels which need to be translated
  jsonfile.readFile(`i18n/${moduleName}/en_US.json`, (err, allLabelsObj) => {
    let labelsExtracted = {};
    const extractedFilePath = `i18n/${moduleName}/en_US_${moduleName}.json`;
    LOCALES.map(filename => {
      const localeFile = `i18n/${(filename === SPANISH_LOCALE_ID) ? SPANISH_FILENAME : filename}.json`;
      jsonfile.readFile(localeFile, (err, localesObj) => {
        if (err) {
          console.log(err);
          return;
        }

        let labelsToWrite = _reduce(allLabelsObj, (acc, obj, labelKey)=> {
          const localeLabel = localesObj[labelKey],
            englishLabel = allLabelsObj[labelKey];

          //If label does not exist add, otherwise use the local one
          if (_isUndefined(localeLabel) || localeLabel === englishLabel) {
            acc[labelKey] = englishLabel;
          }
          return acc;
        }, {});
        labelsExtracted = Object.assign(labelsExtracted, labelsToWrite);
        console.log(`Extracting Json labels for ${filename}`);
        jsonfile.writeFile(extractedFilePath, labelsExtracted);
      });
    });
  });
});
